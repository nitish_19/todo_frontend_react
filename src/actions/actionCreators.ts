import { 
  ADD_TODO,
  REMOVE_ALL_TODOS,
  REMOVE_TODO,
} from '../constants/actionTypes';

import { Todo, TodoAction } from '../API';

export const addTodo = (todo: Todo) => {
  const action: TodoAction = {
    type: ADD_TODO,
    payload: todo,
  }
  return action;
}

export const removeTodo = (todo: Todo) =>  {
  const action: TodoAction = {
    type: REMOVE_TODO,
    payload: todo,
  }
  return action;
}

export const removeAllTodos = () => {
  const todo: Todo = {_id:'',title:'', description:''};
  const action: TodoAction = {
    type: REMOVE_ALL_TODOS,
    payload: todo,
  }
  return action;
}

