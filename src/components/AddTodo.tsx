import React, { useState, FormEvent } from 'react';
import { makeStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { SaveTodoBody, Todo } from '../API';
import { postTodoData } from '../services/FetchNodeServices'; 
import swal from 'sweetalert';
import TodoList from './TodoList';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { addTodo } from '../actions/actionCreators';
import Header from './Header';

// styles for the AddTodo component
const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    padding: 10,
  },
  subdiv: {
    width: 300,
    padding: 10,
    borderRadius: 10,
  },
}));

const AddTodo = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const [todoTitle, setTitle] = useState<string>('');
  const [todoDescription, setDescription] = useState<string>('');

  // handler for Saving Todo, it used FetchNodeServices to call backend
  const handleTodoSave = async (e: FormEvent< HTMLButtonElement >) => {
    const todoBody: SaveTodoBody = {
      title: todoTitle,
      description: todoDescription,
    }
    
    const response = await postTodoData(
      'todo/add',
      todoBody,
    );

    if(response?.status === 201) {
      const data = response.data;
      const todo: Todo = {
        _id: data._id,
        title: data.title,
        description: data.description
      }
      // saving todo in redux store by dipatching action
      dispatch(addTodo(todo));

      swal({
        title: "Todo Added Successfully",
        icon: "success",
        dangerMode: false,
      });
    } else if (response?.statusCode === 401) {
      swal({
        title: "Unauthorized: Signin now.",
        icon: "warning",
        dangerMode: true,
      });
      history.push('/user/signin');
    } else {
        swal({
          title: "Failed to add todo",
          icon: "warning",
          dangerMode: true,
        });
    }
  }

  return (
    <>
      <Header loggedUser={true} />
      <div className={classes.root}>
        <div className={classes.subdiv}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12}>
              <TextField
                label="Title"
                fullWidth
                variant="filled"
                value={todoTitle}
                onChange={(e) => setTitle(e.target.value)}
                required
                inputProps={{ maxLength: 30 }}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                id="filled-multiline-static"
                label="Description"
                multiline
                rows={2}
                fullWidth
                value={todoDescription}
                onChange={(e) => setDescription(e.target.value)}
                variant="outlined"
                inputProps={{ maxLength: 55}}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <Button
                variant="contained"
                fullWidth
                onClick={handleTodoSave}
                style={{
                  backgroundColor: "#fbf343",
                }}
              > Save
              </Button>
            </Grid>
          </Grid>
        </div>
      </div>
      <TodoList/>
    </>
  );
}

export default AddTodo;
