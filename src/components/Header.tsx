import React, { FunctionComponent } from 'react';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { postLogout } from '../services/FetchNodeServices';
import swal from 'sweetalert';

// styles for Header component
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    color: '#000'
  },
}));

// Props to manipulate the header changes according to user
// authentication
interface HeaderProps {
  loggedUser: Boolean,
}

const Header: FunctionComponent<HeaderProps> = ({loggedUser}) => {
  const classes = useStyles();
  const history = useHistory();

  const handleSignin = () => {
    history.push({
      pathname: '/user/signin',
    });
  }
  
  const handleSignup = () => {
    history.push({
      pathname: '/user/signup',
    });
  }

  const handleLogout = async () => {
    
    // calling backend server to logout the signed user
    const response = await postLogout('auth/logout');

    if(response?.status === 201) {
      swal({
        title: `Logged out successfully`,
        icon: "success",
        dangerMode: false,
      });

      history.push({
        pathname: '/user/signin',
      });

    } else {
        swal({
          title: "Error Occured: Try again",
          icon: "warning",
          dangerMode: true,
        });
    }
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{backgroundColor: '#f9f251'}}>
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="primary"
            aria-label="menu">
              <MenuIcon />
          </IconButton>

          <Typography variant="h6" className={classes.title}>
            Todo 📝
          </Typography>
          {loggedUser ? 
            <div>
              <Button
                color="secondary"
                style={{backgroundColor: '#fff'}}
                onClick={handleLogout}
              >Logout
              </Button>
            </div> :
            <>
              <Button
                color="secondary"
                style={{backgroundColor: '#fff', marginRight:10}}
                onClick={handleSignup}
              >Sign up
              </Button>

              <Button
                color="secondary"
                style={{backgroundColor: '#fff'}}
                onClick={handleSignin}
              >Sign in
              </Button>
            </>
          }
        </Toolbar>
      </AppBar>
      <Typography style={{ textAlign: 'center'}} component="h1" variant="h5">
        Keep Your Notes Here
      </Typography>
    </div>
  );
}

export default Header;
