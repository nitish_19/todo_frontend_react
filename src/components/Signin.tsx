import React, { useState } from 'react';

import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import swal from 'sweetalert';
import { SigninBody } from '../API';
import { postUserSigninData } from '../services/FetchNodeServices';
import Header from './Header';

// styles for sign in form
const useStyles = makeStyles({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    padding: 10,
  },
  subdiv: {
    width: 350,
    boxShadow: '0 5px 8px 0 rgba(0,0,0,0.2), 0 7px 20px 0 rgba(0,0,0,0.17)',
    height: 170,
    padding: 10,
    borderRadius: 10
  },
});

const  Signin = () => {
  const classes = useStyles();
  const history = useHistory();

  // states for email and password
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [loggedUser, setLoggedUser] = useState<Boolean>(false);
  
  // handler for sign in, it takes email and password as a signin body and 
  // makes a request to `auth/login` on server
  const handleSignin = async () => {
    const body: SigninBody = {
      email: email,
      password: password,
    }

    // Calling backend API using node services
    const response = await postUserSigninData(
      'auth/login',
      body
    );

    if(response?.status === 201) {
      setLoggedUser(true);
      swal({
        title: `Sign in Successful, Welcome ${response.data.name}`,
        icon: "success",
        dangerMode: false,
      });

      history.push({
        pathname: '/todo/add',
        state: {
          data: response.data
        }
      });

    } else {
        swal({
          title: "Invalid Email or Password",
          icon: "warning",
          dangerMode: true,
        });
    }
  }
  return (
    <>
      <Header loggedUser={loggedUser}/>
      <div className={classes.root}>
        <div className={classes.subdiv}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12}>
              <TextField
                label="Email"
                fullWidth
                variant="outlined"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Grid>

            <Grid item xs={12} sm={12}>
              <TextField
                label="Password"
                fullWidth
                variant="outlined"
                value={password}
                type="password"
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Grid>
            <Grid item xs={12} sm={4}></Grid>
            <Grid item xs={12} sm={4} justify="center">
              <Button
                variant="contained"
                fullWidth
                style={{
                  backgroundColor: "#fbf343",
                }}
                onClick={handleSignin}
              >Sign In
              </Button>
            </Grid>
            <Grid item xs={12} sm={4}></Grid>
          </Grid>
        </div>
      </div>
    </>
  );
}

export default Signin;
