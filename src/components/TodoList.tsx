import React, { useState, useEffect } from 'react';
import TodoListItem from './TodoListItem';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import { deleteAllTodoData, getTodoData } from '../services/FetchNodeServices';
import swal from 'sweetalert';
import { useSelector, useDispatch } from 'react-redux';
import { TodoState } from '../API';
import { removeAllTodos } from '../actions/actionCreators';

// styles for TodoList component
const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    padding: 10,
  },
  subdiv: {
    width: 1200,
    boxShadow: '0 5px 8px 0 rgba(0,0,0,0.2), 0 7px 20px 0 rgba(0,0,0,0.17)',
    padding: 10,
    borderRadius: 10
  },
  clearIcon: {
    marginLeft: 1080,
    color: '#f22626'
  }
}));

// interface for Todo type for data from server
interface Todo {
  _id: string,
  title: string,
  description: string,
};

const TodoList = () => {
  const classes = useStyles();
  const [todolistfrombackend, setTodoList] = useState<Todo[]>([]);
  const dispatch = useDispatch();

  // retrieving todo list from backend on refresh
  const fetchTodos = async () => {
    const todos: Todo[] = await getTodoData('todo/all');
    setTodoList(todos);
  }

  // todolist from redux store, initializing it with concatenating with
  // list from backend, 
  let todolist = useSelector((state: TodoState) => state.todos);
  if(todolistfrombackend){
    todolist = [...todolistfrombackend, ...todolist];
  }
  
  // handler for deleting all todos
  const handleDeleteAllTodos = async () => {
    const response = await deleteAllTodoData(
      'todo',
    );
    // If 'response.data.deletedCount' i spositive it means, query has deleted
    // all todos and the list is cleared
    if(response?.data?.deletedCount > 0) {
      dispatch(removeAllTodos());
      swal({
        title: "Todos Cleared Successfully",
        icon: "success",
        dangerMode: false,
      });
    } else {
        swal({
          title: "No Todos Found",
          icon: "warning",
          dangerMode: true,
        });
    }
  }

  // retrieving all todos on browser reload
  useEffect(() => {
    fetchTodos();
  }, []);

  return (
    <>
      <div className={classes.root}>
        <div className={classes.subdiv}>
          <div className={classes.clearIcon}>
            <Button
              variant="contained"
              onClick={handleDeleteAllTodos}
              style={{
                backgroundColor: "#fbf343",
                marginLeft: 15,
              }}
            > 
              <ClearIcon />
            </Button>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
            }}
          >
            {todolist && todolist.map((todo: Todo) => (
              <TodoListItem todo={todo} />
            ))}
          </div>
        </div>
      </div>
    </>
  );
}

export default TodoList;