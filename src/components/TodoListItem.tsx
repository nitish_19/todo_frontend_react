import React, { FunctionComponent } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import swal from 'sweetalert';
import { deleteTodoData } from '../services/FetchNodeServices';
import { useDispatch } from 'react-redux';
import { removeTodo } from '../actions/actionCreators';

// styles for the TodoListItem component
const useStyles = makeStyles({
  root: {
    width: 345,
    height: 200,
    marginLeft: 30,
    marginRight: 20,
    marginTop: 10,
  },
  card: {
    height: 150,
    background: '#f9f383'
  },
  cardActions: {
    background: '#fbf343',
  },
  editicon: {
    marginLeft: 270,
  }
});

interface Todo {
  _id: string,
  title: string,
  description: string,
}

interface TodoListItemProp {
  todo: Todo,
}

const  TodoListItem: FunctionComponent<TodoListItemProp> = ({ todo }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  // handler for delete a todo with its id
  const handleTodoDelete = async () => {
    const response = await deleteTodoData(
      `todo/${todo._id}`,
    );

    if(response?.status === 200) {
      dispatch(removeTodo(todo));
      swal({
        title: "Todo Deleted Successfully",
        icon: "success",
        dangerMode: false,
      });
    } else {
        swal({
          title: "Failed to delete todo",
          icon: "warning",
          dangerMode: true,
        });
    }
  }
  
  return (
    <Card className={classes.root}>
      <div className={classes.card}> 
      <CardActionArea>
        <CardContent>
           
          <Typography gutterBottom variant="h5" component="h2">
            {todo.title}
          </Typography>
          
          <Typography variant="body2" color="textSecondary" component="p">
            {todo.description}
          </Typography>
         
        </CardContent>
      </CardActionArea>
      </div>
      <div className={classes.cardActions}>
        <CardActions className={classes.editicon}>
          <Button onClick={handleTodoDelete}>
            <DeleteIcon />
          </Button>
        </CardActions>
      </div>
  </Card>
  );
}

export default TodoListItem;
