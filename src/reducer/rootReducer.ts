import { ADD_TODO, REMOVE_TODO, REMOVE_ALL_TODOS } from '../constants/actionTypes';
import { TodoAction, TodoState, Todo } from '../API';

const initialState: TodoState = {
  todos: []
}

// root reducer for redux to create store, it takes action object returns the 
// updated state to redux store
const RootReducer = (
  state: TodoState = initialState,
  action: TodoAction): TodoState => {
    switch (action.type){
      case ADD_TODO:
        return {
          ...state,
          todos: [...state.todos, action.payload],
        }
      case REMOVE_TODO:
        return {
          todos: state.todos.filter((todo: Todo) => todo!==action.payload)
        };
      case REMOVE_ALL_TODOS:
        return {
          todos: []
        };  
      default:
        return state;
    }
}

export default RootReducer;
